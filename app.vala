///
/// Program-Name:	Ant.lgbt-App
/// Description: 	A GTK-implementation of my website
/// Author: 		ConfusedAnt (Yanik Ammann)
///	Last Changed: 	04.05.2020 by ConfusedAnt
///	Prerequisites: 	none
/// Build-command:	valac --pkg gtk+-3.0 --pkg webkit2gtk-4.0  app.vala; ./app
///
/// To-Do:			- Vertically center text
///                 - make icons available if no icon-theme that supports them is found
///
///	Versions (version-number, date, author, description):
/// v0.0, 04.05.2020, ConfusedAnt, starting developement
/// v0.1, 04.05.2020, ConfusedAnt, first release
///

using Gtk;
using WebKit;

public class ValaBrowser : Window {

    private const string TITLE = "ConfusedAnt";
    private const string DEFAULT_URL = "https://ant.lgbt/";

    private Regex protocol_regex;

    private WebView web_view;
    private ComboBoxText comboboxtext;

    public ValaBrowser () {
        this.title = ValaBrowser.TITLE;
        set_default_size (800, 600);

        try {
            this.protocol_regex = new Regex (".*://.*");
        } catch (RegexError e) {
            critical ("%s", e.message);
        }

        create_widgets();

        //  this.set_icon_from_file("logo.png"); 
        this.set_default_icon_name("firefox"); 

        this.destroy.connect (Gtk.main_quit);
    }

    private void create_widgets () {
        //  Gtk.Settings.get_default().set("gtk-application-prefer-dark-theme", true);

        // set header parameters
        var header = new HeaderBar ();
        header.title = "Random Task Generator";
        header.show_close_button = true;
        header.spacing = 5;
	    // set window parameters
        window_position = WindowPosition.CENTER;
        set_titlebar(header);
        set_default_size (960, 640);

	    // theme-button
        var home_icon = new Image.from_icon_name("edit-undo", IconSize.SMALL_TOOLBAR); //gtk-refresh
	    var home_button = new ToolButton (home_icon, "Home");
        home_button.clicked.connect(this.web_view.go_back);

        // combo box
        comboboxtext = new ComboBoxText();
        comboboxtext.append_text("Dark");
        comboboxtext.append_text("Nord");
        comboboxtext.append_text("Light");
        comboboxtext.set_active(0); //TODO: get current
        comboboxtext.changed.connect(changeTheme);

        // add to header
        header.add(comboboxtext);
        header.add(home_button);

        // create WebView
        this.web_view = new WebView ();
        var scrolled_window = new ScrolledWindow (null, null);
        scrolled_window.set_policy (PolicyType.AUTOMATIC, PolicyType.AUTOMATIC);
        scrolled_window.add (this.web_view);
        var box = new Box (Gtk.Orientation.VERTICAL, 0);
        box.pack_start (scrolled_window, true, true, 0);
        add (box);
    }


    private void changeTheme() {
        var selection = comboboxtext.get_active_text().down();
        this.web_view.run_javascript(@"setTheme('$selection')");
    }

    public void start () {
        show_all ();
        this.web_view.load_uri (DEFAULT_URL);
    }

    public static int main (string[] args) {
        Gtk.init (ref args);
        var browser = new ValaBrowser();
        browser.start();
        Gtk.main ();
        return 0;
    }
}